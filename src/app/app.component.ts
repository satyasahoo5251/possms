import { Component, Inject } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent {
  animal: any;
  name: any;

  constructor(public dialog: MatDialog) { }
  title = 'pocSms';

  // tslint:disable-next-line: typedef
  alert() {
    alert(`It is a simple alert box,`);
  }
}
